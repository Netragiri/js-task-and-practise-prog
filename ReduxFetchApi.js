const axios = require('axios')

const redux=require('redux')
const createstore=redux.createStore
const thunkMiddleware = require('redux-thunk').default
const applyMiddleware = redux.applyMiddleware

const initialvalues={
    users:[],
    error:'',
    loading:true
}

const fetch_user_req='fetch_user_req'
const fetch_user_success='fetch_user_success'
const fetch_user_error='fetch_user_error'


const fetchReaquest=()=>{
    return{
        type:fetch_user_req,
      
    }
}

const fetchUserSuccess=(users)=>{
    return{
        type:fetch_user_success,
        payload:users
    }
}

const fetchUserError=(error)=>{
    return{
        type:fetch_user_error,
        payload:error
    }
}

const reducer=(state=initialvalues,action)=>{
    switch(action.type){
        case fetch_user_req:
            return{
                ...state,
                loading:true
        }
        case fetch_user_success:
            return{
                loading:false,
                users:action.payload
        }
        case fetch_user_error:
            return{
                loading:false,
                user:[],
                error:action.payload
            }
    }

}



function fetchAPI(){
    return function(dispatch){
        dispatch(fetchReaquest())
        axios('https://jsonplaceholder.typicode.com/users')
        .then(response=>{
            const users=response.data.map(item=>item.id)
            dispatch(fetchUserSuccess(users))
        })
        .catch(error=>{
            dispatch(fetchUserError(error))
        })
    }
}
const store=createstore(reducer,applyMiddleware(thunkMiddleware))
store.subscribe(()=>console.log(store.getState()))
store.dispatch(fetchAPI())